import pandas as pd
from modules.config.settings import DATA_PATH
from modules.verify.target_data import get_target_data, get_file_list


# verify.target_data
def test_get_target_data():
    df = pd.read_excel(DATA_PATH)
    test_data = df.where(pd.notnull(df), None).set_index('file_name').to_dict(orient='index')
    assert test_data == get_target_data()


def test_get_file_list():
    df = pd.read_excel(DATA_PATH)
    test_data = df['file_name'].to_list()
    assert test_data == get_file_list()