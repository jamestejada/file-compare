import os
from datetime import datetime
from modules.mirror.should_copy import Should_Copy
from modules.config.settings import TEST_PATH, MIRROR_PATH, ENCO_PATH


TEST_FILE_NAME = '18081.MP2'
FAKE_TEST_FILE = 'DUMMY_FILE.MP2'


def test_directories():
    assert TEST_PATH.exists()
    assert MIRROR_PATH.exists()
    assert ENCO_PATH.exists()


def test_testmode():
    assert TEST_PATH == ENCO_PATH


# mirror.Should_Copy
def test_should_copy_to_mirror_exist():
    example = Should_Copy(TEST_FILE_NAME)
    exist_result = example.should_copy_to_mirror
    assert exist_result is False


def test_should_copy_to_mirror_not_exist():
    not_there = Should_Copy(FAKE_TEST_FILE)
    not_exist_result = not_there.should_copy_to_mirror
    assert not_exist_result is True


def test_get_mod_time():
    file_path = TEST_PATH.joinpath(TEST_FILE_NAME)
    test_mod_time = datetime.fromtimestamp(os.path.getmtime(file_path))
    assert test_mod_time == Should_Copy.get_mod_time(file_path)