import json
from datetime import datetime
from modules.output.repeats import Repeat_Handler
from modules.config.settings import TEST_IGNORE_DICT_PATH

TEST_FILE_NAME = '18081.MP2'
TEST_DICT = {
    '18081.MP2': {
        'file_name': TEST_FILE_NAME,
        'notified_time': datetime.now().strftime('%Y%m%d %H:%M:%S')
    }
}
EXPIRED_DICT = {
    '18081.MP2': {
        'file_name': '18081.MP2',
        'notified_time': '20200901 16:07:37'
        }
    }


# modules.output.repeats
def get_Repeat_Handler_instance(input_dict=None):
    test_dict = input_dict if input_dict else TEST_DICT
    return Repeat_Handler(
        file_name=TEST_FILE_NAME,
        interval_text='weekly',
        test_dict=test_dict
    )


def get_test_dict_from_file():
    with open(TEST_IGNORE_DICT_PATH, 'r') as infile:
        return json.load(infile)


def test_set_IGNORE_DICT():
    Repeat_Handler.set_IGNORE_DICT()
    ignore_dict = Repeat_Handler.IGNORE_DICT

    test_value = get_test_dict_from_file()
    
    assert test_value == ignore_dict


def test_add_to_ignore_dict():
    repeat_checker = get_Repeat_Handler_instance()
    repeat_checker.add_to_ignore_dict()

    assert repeat_checker.IGNORE_DICT == TEST_DICT


def test_should_ignore_expired():
    repeat_checker = get_Repeat_Handler_instance(input_dict=EXPIRED_DICT)

    repeat_checker.just_added = False
    assert not repeat_checker.just_added
    assert repeat_checker.ignore_expired()
    assert not repeat_checker.should_ignore


def test_should_ignore_not_expired():
    repeat_checker = get_Repeat_Handler_instance(input_dict=TEST_DICT)

    repeat_checker.just_added = False
    assert not repeat_checker.just_added
    assert not repeat_checker.ignore_expired()
    assert repeat_checker.should_ignore

# test get_notified_time
def test_get_notified_time_expired():
    repeat_checker = get_Repeat_Handler_instance(input_dict=EXPIRED_DICT)
    test_timestamp = EXPIRED_DICT.get(TEST_FILE_NAME).get('notified_time')
    instance_timestamp = repeat_checker.get_notified_time().strftime('%Y%m%d %H:%M:%S')

    assert test_timestamp == instance_timestamp

def test_get_notified_time():
    repeat_checker = get_Repeat_Handler_instance()
    test_timestamp = TEST_DICT.get(TEST_FILE_NAME).get('notified_time')
    instance_timestamp = repeat_checker.get_notified_time().strftime('%Y%m%d %H:%M:%S')
    
    assert test_timestamp == instance_timestamp


def test_remove_from_IGNORE_DICT():
    repeat_checker = get_Repeat_Handler_instance()
    Repeat_Handler.remove_from_IGNORE_DICT(TEST_FILE_NAME)
    # repeat_checker.remove_from_IGNORE_DICT(TEST_FILE_NAME)

    assert Repeat_Handler.IGNORE_DICT == {}
    assert repeat_checker.IGNORE_DICT == {}


def test_write_IGNORE_DICT():
    repeat_checker = get_Repeat_Handler_instance(EXPIRED_DICT)
    Repeat_Handler.write_IGNORE_DICT()

    with open(TEST_IGNORE_DICT_PATH, 'r') as infile:
        read_dict = json.load(infile)

    assert read_dict == Repeat_Handler.IGNORE_DICT
