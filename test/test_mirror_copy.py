import os
from modules.config.settings import TEST_PATH, MIRROR_PATH, ENCO_PATH
from modules.mirror.mirror_copy import Mirror_Copy, Checksum


TEST_FILE_NAME = '18081.MP2'
FAKE_TEST_FILE = 'DUMMY_FILE.MP2'


# mirror.mirror_copy.Mirror_Copy
def test_check_exist():
    file_exists = Mirror_Copy([TEST_FILE_NAME])
    assert file_exists.check(TEST_FILE_NAME) == False


def test_check_not_exist():
    not_there = Mirror_Copy([FAKE_TEST_FILE])
    assert not_there.check(FAKE_TEST_FILE) == True


def remove_fake_file(full_path):
    try:
        os.remove(full_path)
    except FileNotFoundError:
        pass

def test_copy():
    not_there = Mirror_Copy([FAKE_TEST_FILE])
    not_there.copy(FAKE_TEST_FILE)
    full_path = MIRROR_PATH.joinpath(FAKE_TEST_FILE)
    result = full_path.exists()
    remove_fake_file(full_path)
    assert result


# mirror.mirror_copy.Checksum
def test_verified():
    not_there = Mirror_Copy([FAKE_TEST_FILE])
    not_there.copy(FAKE_TEST_FILE)
    full_path = MIRROR_PATH.joinpath(FAKE_TEST_FILE)
    assert full_path.exists()

    result = Checksum.verified(FAKE_TEST_FILE)
    remove_fake_file(full_path)
    assert result
