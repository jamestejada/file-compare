from mutagen.mp3 import MP3
from modules.verify.compare import Compare
from modules.config.settings import TEST_PATH


TEST_FILE_NAME = '18081.MP2'
TEST_TARGET_DICT = {
    18081: {   
        'add_to': None,
        'segment_name': 'KZAP',
        'target_duration': 3599,
        'target_samplerate': 44100
        }
    }


# verify.compare.Compare
def get_instance_Compare():
    return Compare(TEST_TARGET_DICT, [TEST_FILE_NAME])


def get_Compare_output_dict():
    example = get_instance_Compare()
    example.compare_all()
    return example.to_dict()[TEST_FILE_NAME]


def test_target_duration():
    result = get_Compare_output_dict()['target_duration']
    assert result == 3599


def test_target_samplerate():
    result = get_Compare_output_dict()['target_samplerate']
    assert result == 44100


def test_target_segment_name():
    result = get_Compare_output_dict()['segment_name']
    assert result == 'KZAP'


def test_target_add_to():
    result = get_Compare_output_dict()['add_to']
    assert result is None


def test_duration_output():
    test_duration = int(MP3(TEST_PATH.joinpath(TEST_FILE_NAME)).info.length)
    result = get_Compare_output_dict()['actual_duration']
    assert test_duration == result


def test_samplerate_output():
    test_samplerate = int(MP3(TEST_PATH.joinpath(TEST_FILE_NAME)).info.sample_rate)
    result = get_Compare_output_dict()['actual_samplerate']
    assert test_samplerate == result

#       SAMPLE OUTPUT from Compare.to_dict()
#       {
#           '18081.MP2': {
#               'file_name': '18081.MP2', 
#               'duration_ok': True, 
#               'samplerate_ok': True, 
#               'add_to': None, 
#               'segment_name': 'KZAP',
#               'target_duration': 3599,
#               'target_samplerate': 44100,
#               'actual_duration': 3594,
#               'actual_samplerate': 44100
#               }
#       }