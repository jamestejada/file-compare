from mutagen.mp3 import MP3
from modules.config.settings import TEST_PATH
from modules.verify.file_info import File_Info

# TO DO: 
#   - Add tests for added segments (with add_to not None)

TEST_FILE_NAME = '18081.MP2'


# verify.file_info.File_Info
def get_instance_File_Info():
    return File_Info(TEST_FILE_NAME)


def test_File_Info_get_extension():
    example = get_instance_File_Info()
    assert example.extension == '.MP2'


def test_File_Info_get_duration():
    test_duration = int(MP3(TEST_PATH.joinpath(TEST_FILE_NAME)).info.length)
    example = get_instance_File_Info()

    assert example.actual_duration == test_duration


def test_File_Info_get_samplerate():
    example = get_instance_File_Info()
    assert example.actual_samplerate == 44100