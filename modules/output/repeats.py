import json
from datetime import datetime, timedelta
from dateutil.parser import parse
from modules.config.settings import (
        TESTING,
        DAILY_INTERVAL, 
        WEEKLY_INTERVAL,
        IGNORE_DICT_PATH,
        TEST_IGNORE_DICT_PATH
    )


class Repeat_Handler:
    """ Handles repeated discovery of non-compliant file
    so that this program doesn't spam Slack.
    """
    IGNORE_DICT = {}
    IGNORE_DICT_FILE_PATH = TEST_IGNORE_DICT_PATH if TESTING else IGNORE_DICT_PATH
    INTERVAL_CONVERT_TEXT = {'daily': DAILY_INTERVAL, 'weekly': WEEKLY_INTERVAL}

    def __init__(self, file_name: str, interval_text='daily', test_dict=None):
        self.file_name = file_name
        self.just_added = False
        self.test_dict = test_dict
        self.set_test_dict()
        self.notified_time = self.get_notified_time()
        self.interval = timedelta(
            days=self.INTERVAL_CONVERT_TEXT.get(interval_text)
        )

    def set_test_dict(self):
        """ test module that sets class variable IGNORE_DICT to the
        inputted test_dict
        """
        if self.test_dict:
            Repeat_Handler.IGNORE_DICT = self.test_dict

    @staticmethod
    def set_IGNORE_DICT():
        """ sets class variable IGNORE_DICT by reading file
        """
        if Repeat_Handler.IGNORE_DICT_FILE_PATH.exists():
            with open(Repeat_Handler.IGNORE_DICT_FILE_PATH, 'r') as infile:
                Repeat_Handler.IGNORE_DICT = json.load(infile)

    def get_notified_time(self):
        """ Gets last time team was notified of a self.file_name's
        non-compliance. If self.file_name is not found in self.ignore_list,
        the file_name and notification time are written into the Ignore List file.
        """
        # Maybe use dict.setdefault here instead.
        try:
            return parse(self.IGNORE_DICT[self.file_name]['notified_time'])
        except KeyError:
            return self.add_to_ignore_dict()

    def add_to_ignore_dict(self):
        """ adds another entry into self.ignore_dict
        """
        timestamp = datetime.now().strftime('%Y%m%d %H:%M:%S')
        self.IGNORE_DICT[self.file_name] = {
            'file_name': self.file_name,
            'notified_time': timestamp
        } if not self.test_dict else self.test_dict.get(self.file_name)
        
        self.just_added = True
        return parse(timestamp)

    @staticmethod
    def write_IGNORE_DICT():
        """ writes ignore list to a file for storage.
        """
        # should be accessible from outside to allow writing of
        # self.IGNORE_DICT to file after adding all items.
        with open(Repeat_Handler.IGNORE_DICT_FILE_PATH, 'w+') as outfile:
            json.dump(Repeat_Handler.IGNORE_DICT, outfile)

    @staticmethod
    def remove_from_IGNORE_DICT(file_name: str):
        """ static method that removes an entry from IGNORE_DICT
        for file_name.
        """
        try:
            Repeat_Handler.IGNORE_DICT.pop(file_name)
        except KeyError:
            pass

    @property
    def should_ignore(self) -> bool:
        """ determines if a notification about self.file_name's
        non-compliance should be ignored.
        """
        if self.just_added:
            return False

        if self.ignore_expired():
            if not self.test_dict:
                self.IGNORE_DICT.pop(self.file_name)
                self.add_to_ignore_dict()
            return False

        return True

    def ignore_expired(self):
        return (datetime.now() - self.notified_time) > self.interval


class Repeat_Handler_Context:
    def __enter__(self):
        Repeat_Handler.set_IGNORE_DICT()

    def __exit__(self, exc_type, exc_value, exc_traceback):
        Repeat_Handler.write_IGNORE_DICT()


def remove_from_ignore(file_name: str):
    Repeat_Handler.remove_from_IGNORE_DICT(file_name)

