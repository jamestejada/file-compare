import logging
from contextlib import contextmanager
from modules.config.settings import LOG_PATH, LOG_LEVEL

log_level = {
    'debug': logging.DEBUG,
    'info': logging.INFO
}.get(LOG_LEVEL)


@contextmanager
def initialize_logger():
    disable_urllib_logger()

    # initialize logging
    logging.basicConfig(
        filename=LOG_PATH,
        filemode='a',
        format='%(asctime)s %(name)s %(levelname)s %(message)s',
        datefmt='%H:%M:%S',
        level=log_level
    )
    bot_logger = logging.getLogger('OPS_VERIFY_BOT')
    bot_logger.info('------------Ops Bot started------------')
    try:
        yield bot_logger
    finally:
        bot_logger.info('------------Ops Bot Finished------------')


def get_logger():
    return logging.getLogger('OPS_VERIFY_BOT')


def disable_urllib_logger():
    """ Stops urllib3 (used by requests) from adding debug
    entries into our logs.
    """
    logging.getLogger('urllib3').setLevel(logging.WARNING)