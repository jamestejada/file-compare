
import json
import paramiko
import subprocess
from modules.output.logger import get_logger
from modules.config.settings import (
    LOCAL_OUTPUT_PATH, REMOTE
    )


def write_output(output_dict):
    """ Writes output file with results of file checking from production machine.
    """
    with open(LOCAL_OUTPUT_PATH.joinpath('output.json'), 'w+') as outfile:
        json.dump(output_dict, outfile)


def read_remote_output():
    """ Reads in the results of file checking on production machine.
    """
    with open(LOCAL_OUTPUT_PATH.joinpath('output.json'), 'r') as infile:
        return json.load(infile)


def scp_from_remote() -> bool:
    """ Copies file from remote production machine.

        NOTE: You will need to set up ssh keys.
    """
    result = subprocess.run(
        [
            'scp',
            '-P', REMOTE.port,
            f'{REMOTE.user}@{REMOTE.host}:{REMOTE.path}',
            LOCAL_OUTPUT_PATH]
        )
    return (result.returncode == 0)

def remote_run():
    """ sends command through ssh to run program on remote machine.
    """

    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    
    client.connect(username=REMOTE.user, hostname=REMOTE.host, port=REMOTE.port)

    stdin, stdout, stderr = client.exec_command(REMOTE.execute)

    error_tag = 'REMOTE_ERROR: '
    remote_error = f'{error_tag}{stderr.read().decode()}'
    remote_output = f'REMOTE OUTPUT: {stdout.read().decode()}'

    if len(remote_error) > len(error_tag):
        # An error was raised on remote machine
        get_logger().debug(remote_error)
        return remote_error
    
    get_logger().info(remote_output)
    return remote_output