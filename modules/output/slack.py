import json
import requests
from datetime import datetime, timedelta
from modules.output.repeats import (
    remove_from_ignore,
    Repeat_Handler,
    Repeat_Handler_Context
    )
from modules.output.logger import get_logger
from modules.config.settings import (
    SLACK_TEST_WEBHOOK,
    ROBOT_RUN,
    TESTING
    )


"""
WEBHOOK PAYLOAD EXAMPLE
{
  "file_name": "Example text",
  "duration_ok": "Example text",
  "samplerate_ok": "Example text",
  "segment_name": "Example text",
  "actual_duration_formatted": "Example text"
}
"""

def post_to_slack(file_name: str, duration_ok: bool,
                    samplerate_ok: bool, segment_name: str, actual_duration: int):
    payload = json.dumps({
        "file_name": file_name,
        "duration_ok": 'OK' if duration_ok else 'Not Good',
        "samplerate_ok": 'OK' if samplerate_ok else 'Not Good',
        "segment_name": segment_name,
        "actual_duration_formatted": format_duration(actual_duration)
    })
    slack_hook = requests.post(SLACK_TEST_WEBHOOK, data=payload)
    return slack_hook.status_code


def process_results_for_slack(results_dict):
    """ Sends info for non-compliant files to slack to alert team.
    """
    responses = []

    with Repeat_Handler_Context():
        for file_name, result_obj in results_dict.items():
            if result_obj.get('duration_ok') and result_obj.get('samplerate_ok'):
                remove_from_ignore(file_name)
            else:
                one_file = get_Repeat_Handler_instance(result_obj)

                if not one_file.should_ignore:
                    responses.append(
                        post_to_slack(
                            file_name,
                            result_obj.get('duration_ok'),
                            result_obj.get('samplerate_ok'),
                            result_obj.get('segment_name'),
                            result_obj.get('actual_duration')
                        )
                    )

    return responses


def get_Repeat_Handler_instance(result_obj: dict) -> object:
    get_logger().debug(f'NOT COMPLIANT: {result_obj}')
    return Repeat_Handler(
        result_obj.get('file_name'),
        interval_text=result_obj.get('recurs')
        )


def responses_ok(responses: list) -> bool:
    all_200 = all(status == 200 for status in responses)
    bot_logger = get_logger()
    bot_logger.info(f'Responses OK? {all_200}')
    bot_logger.info(f'Slack HTTP Responses: {responses}')
    if not all_200:
        bot_logger.warn('One or more requests to Slack Webhook have failed.')
        return False
    return True


def format_duration(actual_duration: int) -> str:
    return str(timedelta(seconds=actual_duration))


# not used right now.
def run_confirmation():
    payload = json.dumps({"datetime": get_timestamp()})
    slack_hook = requests.post(ROBOT_RUN, data=payload)
    return slack_hook.status_code
