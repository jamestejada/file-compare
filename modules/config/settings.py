import os
import sys
from pathlib import Path
from datetime import datetime
from dotenv import load_dotenv


TEST_FLAGS = ['testing', 'test']
LOCAL_FLAGS = ['local']


def path_verify(path_list: list):
    dir_path = Path.cwd().joinpath(*path_list)
    dir_path.mkdir(parents=True, exist_ok=True)
    return dir_path


def get_date_string() -> str:
    return datetime.now().strftime('%m%d%Y')


def check_flags(flags) -> bool:
    for arg in flags:
        if arg in sys.argv[1:]:
            return True
    return False


# get environment variables from .env file
_env_path = Path.cwd().joinpath('modules','config', '.env')
load_dotenv(dotenv_path=_env_path)

TESTING = check_flags(TEST_FLAGS)
LOCAL_ONLY = check_flags(LOCAL_FLAGS)
LOG_LEVEL = 'info'
MACHINE = os.getenv('MACHINE')

class REMOTE:
    port = os.getenv('REMOTE_PORT')
    user = os.getenv('REMOTE_USER')
    host = os.getenv('REMOTE_HOST')
    path = os.getenv('REMOTE_OUTPUT_PATH')
    execute = os.getenv('REMOTE_EXECUTE_PATH')

LOCAL_OUTPUT_PATH = Path.cwd().joinpath('modules', 'output')

# Slack Webhooks
SLACK_TEST_WEBHOOK = os.getenv('SLACK_WEBHOOK')
ROBOT_RUN = os.getenv('SLACK_RUN_CONFIRMATION')

# Paths
TEST_PATH = path_verify(['audio', 'TEST_DROPBOX'])
ENCO_PATH = TEST_PATH if TESTING else Path('/mnt/audio')
MIRROR_PATH = path_verify(['audio', 'DROPBOX_MIRROR'])
DATA_PATH = path_verify(['data']).joinpath('target_data.xlsx')
LOG_PATH = path_verify(['logs']).joinpath(f'ops-verify-bot-{get_date_string()}.log')
IGNORE_DICT_PATH = Path.cwd().joinpath('modules', 'output', 'repeat.json')
TEST_IGNORE_DICT_PATH = Path.cwd().joinpath('modules', 'output', 'test_repeat.json')

# Add 5 seconds to account for some discrepancy
# between metadata duration and actual duration
DURATION_TOLERANCE = 10

# time period in which non-compliant files will be ignored (in days)
DAILY_INTERVAL = 1
WEEKLY_INTERVAL = 1
