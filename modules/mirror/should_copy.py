import os
from modules.config.settings import ENCO_PATH, MIRROR_PATH
from datetime import datetime


class Should_Copy:
    """ This class determines if a specific file should be copied over.
    If the file in the copy folder is older than the file in the target
    folder, the file will be copied to the copy folder.

    NOTE: input file_name should include the extension
    """
    SOURCE = ENCO_PATH
    DESTINATION = MIRROR_PATH

    def __init__(self, file_name: str):
        self.destination_path = self.DESTINATION.joinpath(file_name)
        self.source_path = self.SOURCE.joinpath(file_name)

    @property
    def should_copy_to_mirror(self) -> bool:
        """ Returns true if copy_path is less than dropbox_path
        """
        if not self.destination_path.exists():
            return self.source_path.exists()
        return (
            Should_Copy.get_mod_time(self.destination_path)
            < Should_Copy.get_mod_time(self.source_path)
            )

    @staticmethod
    def get_mod_time(file_path):
        """ Returns modified time of file
        """
        return datetime.fromtimestamp(os.path.getmtime(file_path))
