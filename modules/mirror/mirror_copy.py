import shutil
from modules.mirror.should_copy import Should_Copy
from modules.mirror.checksum import Checksum
from modules.config.settings import ENCO_PATH, MIRROR_PATH


class Mirror_Copy:
    """ This class loops over list of file names checking if enco files are newer than mirror files. 
    If they are, files are copied to the mirror.

    NOTE: file_list should be a list of file names that include the extensions
    """
    SOURCE_PATH = ENCO_PATH
    MIRROR_PATH = MIRROR_PATH

    def __init__(self, file_list: list):
        self.list = file_list
        self.verified = Checksum.verified

    def process_list(self):
        for each_file in self.list:
            self.check_and_copy(each_file)

    def check_and_copy(self, file_name):
        if self.check(file_name):
            print(f'copying: {file_name}')
            self.copy(file_name)
            if self.verified(file_name):
                print(f'verified: {file_name}')
    
    def check(self, file_name):
        """ checks wether or not file should be copied to mirror
        """
        return Should_Copy(file_name).should_copy_to_mirror
    
    def copy(self, file_name):
        """ copies file to mirror
        """
        shutil.copy(
            str(self.SOURCE_PATH.joinpath(file_name).absolute()),
            str(self.MIRROR_PATH.joinpath(file_name).absolute())
        )
