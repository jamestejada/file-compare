from hashlib import md5
from modules.config.settings import ENCO_PATH, MIRROR_PATH


class Checksum:
    """ A collection of static methods for file verification.
    """
    SOURCE_PATH = ENCO_PATH
    MIRROR_PATH = MIRROR_PATH

    @staticmethod
    def verified(file_name) -> bool:
        """ returns true if files on dropbox and copy files
        have the same hash.
        """
        path_tuple = Checksum.get_paths(file_name)
        return Checksum.compare_hash(*path_tuple)
    
    @staticmethod
    def get_paths(file_name):
        """ returns a tuple of paths to files to hash
        """
        return (
            Checksum.SOURCE_PATH.joinpath(file_name),
            Checksum.MIRROR_PATH.joinpath(file_name)
            )

    @staticmethod
    def compare_hash(path1, path2) -> bool:
        """compares hashes given file paths """
        return Checksum.hash_one(path1) == Checksum.hash_one(path2)
    
    @staticmethod
    def hash_one(file_path) -> str:
        with open(file_path, 'rb') as one_file:
            file_contents = one_file.read()
            return md5(file_contents).hexdigest()
