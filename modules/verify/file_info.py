from pathlib import Path
from mutagen.mp3 import MP3
from mutagen.wave import WAVE
from modules.config.settings import MIRROR_PATH


class File_Info:
    """ class that instantiates objects representing a single file
    """
    WAV_EXT = ['.wav']
    MP3_EXT = ['.mp2', '.mp3']
    SOURCE_PATH = MIRROR_PATH

    def __init__(self, file_name, add_to=None):
        self.file_name = file_name
        self.add_to = add_to    #  list that does not contain extension
        self.extension = self.get_extension()
        self.actual_duration = self.get_duration()
        self.actual_samplerate = self.get_samplerate()

    def get_extension(self):
        """ returns the file extension of self.file_name
        """
        return Path(self.file_name).suffix

    def get_duration(self):
        """ returns duration of segment (or sum of segments) in seconds
        """
        if self.add_to is None:
            return self._get_one_duration(self.file_name)
        return self._add_segments()
    
    def get_samplerate(self):
        """ returns sample rate of file
        """
        file_path = self.get_path(self.file_name)

        if self.extension.lower() in self.WAV_EXT:
            return WAVE(file_path).info.sample_rate
        if self.extension.lower() in self.MP3_EXT:
            return MP3(file_path).info.sample_rate

    def _add_segments(self):
        """ adds segments together and returns total duration in seconds
        """
        duration_list = list(map(self._get_one_duration, self.add_to))
        duration_list.append(self._get_one_duration(self.file_name))
        return sum(duration_list)

    def _get_one_duration(self, one_file_name):
        """ gets the duration of one file
        """
        suffix = Path(str(one_file_name)).suffix
        extension = self.extension if suffix == '' else suffix

        if extension.lower() in self.WAV_EXT:
            return self.wav_duration(one_file_name)
        if extension.lower() in self.MP3_EXT:
            return self.mp3_duration(one_file_name)
    
    def get_path(self, file_root):
        """ returns path to file in mirror
        """
        if Path(str(file_root)).suffix == '':
            return self.SOURCE_PATH.joinpath(f'{file_root}{self.extension}')
        return self.SOURCE_PATH.joinpath(file_root)

    def wav_duration(self, file_root):
        """ returns the duration of a single wav file.
        """
        file_path = self.get_path(file_root)
        return int(WAVE(file_path).info.length)

    def mp3_duration(self, file_root):
        """ returns the duration of a single mp3 file.
        """
        file_path = self.get_path(file_root)
        if file_path.exists():
            return int(MP3(file_path).info.length)

    def mp2_duration(self, file_root):
        """ returns the duration of a single mp2 file.
        (wrapper of mp3_duration)
        """
        return self.mp3_duration(file_root)

    def to_dict(self):
        """ returns a dictionary with file info
        """
        return {
            'actual_duration': self.actual_duration,
            'actual_samplerate': self.actual_samplerate
        }