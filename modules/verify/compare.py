from pathlib import Path
from modules.config.settings import DURATION_TOLERANCE
from modules.verify.file_info import File_Info
from modules.verify.target_data import get_target_data


class Compare:
    """ Compares actual file data with target file data
    """
    TOLERANCE = DURATION_TOLERANCE

    def __init__(self, data_dict: dict, dir_list: list):
        self.data_dict = data_dict
        self.dir_list = dir_list
        self.output_dict = {}
    
    def compare_all(self):
        """ compares all files in dir_list
        """
        for file_name in self.dir_list:
            self.compare_one(file_name)
    
    def compare_one(self, file_name):
        """ compares one file with target data.
        Adds all info to a structured 
        """
        # Should I separate out a class to compare one file?
        file_root = int(Path(file_name).stem)
        target_dict = self.data_dict.get(file_root)
        add_to = self.process_add_to(target_dict.get('add_to'))
        target_duration = target_dict.get('target_duration')
        target_samplerate = target_dict.get('target_samplerate')
        file_obj = File_Info(file_name, add_to=add_to)

        combined_dict = {
            'file_name': file_name,
            'duration_ok': self._compare_duration(target_duration, file_obj.actual_duration),
            'samplerate_ok': self._compare_samplerate(target_samplerate, file_obj.actual_samplerate),
            **target_dict,
            **file_obj.to_dict()
        }
        combined_dict['add_to'] = add_to

        self.output_dict[file_name] = combined_dict

    def process_add_to(self, add_to_str):
        if add_to_str is None:
            return None
        add_to_list = str(add_to_str).split(',')
        clean_list = list(map(self.string_to_int, add_to_list))
        return clean_list

    def string_to_int(self, cut_string) -> int:
        if cut_string == 'None':
            return None
        cut_string = cut_string.strip()
        return int(cut_string)

    def _compare_duration(self, target_duration, actual_duration):
        """ compares actual with target duration given tolerance
        """
        return (
            (target_duration - self.TOLERANCE)
            <= actual_duration
            < (target_duration + self.TOLERANCE)
            )

    def _compare_samplerate(self, target_samplerate, actual_samplerate):
        """ compares actual with target samplerate
        """
        return target_samplerate == actual_samplerate

    def to_dict(self):
        """ Outputs compare results and file information
        for all files into dictionary
        """ 
        return self.output_dict


# ENTRY POINT for this module
def compare_files(file_list):
    """ Compares files in list with documented target data. 
    Returns a dictionary with information on files.
    """
    comparer = Compare(get_target_data(), file_list)
    comparer.compare_all()
    return comparer.to_dict()