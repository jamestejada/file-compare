import pandas as pd
from modules.config.settings import DATA_PATH


def get_target_data():
    data_file = DATA_PATH
    df = pd.read_excel(data_file)
    return df.where(pd.notnull(df), None).set_index('file_name').to_dict(orient='index')


def get_file_list():
    df = pd.read_excel(DATA_PATH)
    return df['file_name'].to_list()
