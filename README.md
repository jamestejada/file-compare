# **CapRadio Ops Bot**

<p>This is a bot that checks various files used at Capital Public Radio to ensure their integrity. This check is done after files are edited and ingested into the automation system in an attempt to catch non-compliant files before they air. </p>

## Features
- Checks Samplerate and Duration of each file against database of target values.
- Copies files from automation directory and hashes to ensure integrity.
- Integrates with Slack to notify ops team, and coordinates efforts to correct problems. 

## Environment
This is application is set to run on two different machines. One checks the fileserver on an internal network, as second notifies the Ops team in the case of a corrupted or mis-timed file. Both machines are currently running on Ubuntu 20.04 LTS. 

## Setup

### For `Slacker` Machine
- You will need to set up a workflow in slack that incorporates a webhook. Take this webhook url and save it in  ./modules/config/.env (you will need to create this file yourself):
    ```
    SLACK_WEBHOOK=https://hooks.slack.com/workflows/{SECRET_STRING}
    ```
- To gain access to the production machine you will need to generate ssh keys and then upload them to the remote production machine. Make sure that you do not disable password login until you have copied the ssh keys to the production machine.
    ```
    $ ssh-keygen -t rsa -b 4096
    ...
    $ ssh-copy-id -i ~/.ssh/mykey user@host
    ```

### For `Production` Machine
- To mount the fileserver you will need the `cifs-utils` package and to allow for remote access you will need the `openssh-server` package.
    ```
    $ apt install cifs-utils
    ...
    $ apt install openssh-server
    ```
- Create a mount folder called `m` and mount the file server onto it.
    <p>NOTE: You may want to create a credentials file if your fileshare requires credentials.</p>

    ```
    $ mkdir /mnt/m
    $ mount -t cifs //{servername}/{sharename} /mnt/m -o credentials=/home/user/.credentials,vers=2.0
    ```
- Configure the machine's firewall for both access and security. Follow good ssh hardening practices. 

### For *Both* Machines
- Make sure `run.py` is made executable
    ```
    $ chmod +x run.py
    ```

- You will need to assign another environment variable in the .env file that designates the current machine. This will control what modules will be executed from `run.py`.
    ```
    MACHINE=slacker
    ```
    this `MACHINE` environment variable should be set to either `'slacker'` for the notification machine or `'production'` for the file checking machine.

- Create a virtual environment by running the `environ` script.
    ```
    $ . environ
    ```

- Within the virtual environment, use the Python package manager, pip, to install the needed dependencies from the requirements.txt file.
    ```
    $ pip install -r requirements.txt
    ```

- Run tests to make sure everything is set. Run tests from `run.py` by passing "*test*" as a parameter
    ```
    $ python3 run test
            or
    $ ./run.py test
    ```

## Running the Robot
To run the robot simply execute `run.py`

```
$ ./run.py
    or
$ python3 -m run
```
or just set up your cron job to run `run.py` within the python virtual environment using a script such as the one below.

```bash
#! /bin/bash

# File: cronrun

cd "/path/to/repo/file-compare"
source "venv/bin/activate"

python3 -m run
```

If you run it this way using a cron job (and you probably should) make sure that you also make this script executable. 

```
$ chmod +x cronrun
```

Please Note that you will need to run this on both the `production` machine and then the `slacker` machine. The application will SCP the output json file from the remote production machine.
