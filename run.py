
import pytest
from modules.verify.compare import compare_files
from modules.mirror.mirror_copy import Mirror_Copy
from modules.output.logger import initialize_logger, get_logger
from modules.output.slack import process_results_for_slack, responses_ok
from modules.output.remote import (
    write_output, read_remote_output, scp_from_remote, remote_run
    )
from modules.verify.target_data import get_file_list
from modules.config.settings import TESTING, MACHINE, LOCAL_ONLY


""" This is the entry point for the file verification robot. 
If you are setting this up, make sure that this file is executable.
"""

""" 
TO DO: 

**Tests**
    - Create a test for adding up duration of several files. 
    - Create test for Mirror_Copy that copies a newer file
      into mirror over old file.
    - Use a smaller file for the test file. 18081.MP2 is pretty large.
"""


def normal_run():
    # Start Logging
    with initialize_logger() as bot_logger:

        file_list = get_files_to_copy()
        bot_logger.debug(f'Files to Copy: {file_list}')

        result_dict = compare_files(file_list)
        bot_logger.debug(f'Results: {result_dict}')

        slack_response_list = process_results_for_slack(result_dict)
        responses_ok(slack_response_list)


def production_run():
    with initialize_logger() as bot_logger:

        file_list = get_files_to_copy()
        bot_logger.debug(f'Files to Copy: {file_list}')

        result_dict = compare_files(file_list)
        bot_logger.debug(f'Results: {result_dict}')

        write_output(result_dict)



def slack_run():
    with initialize_logger() as bot_logger:

        # if local only flag passed, do not perform remote run.
        if not LOCAL_ONLY:
            print(remote_run())

        # Get json file from remote machine (production machine)
        copy_result = scp_from_remote()
        bot_logger.info(f'SECURE COPY OK? {copy_result}')

        # convert to dict
        result_dict = read_remote_output()

        slack_response_list = process_results_for_slack(result_dict)
        responses_ok(slack_response_list)


def get_files_to_copy() -> list:
    """ returns list of files that need to be checked for compliance
    """
    stem_list = get_file_list()
    file_list = list(map(lambda name: f'{name}.MP2', stem_list))

    copier = Mirror_Copy(file_list)
    copier.process_list()

    return file_list


def main():
    try:
        if TESTING:
            pytest.main()
        elif MACHINE == 'slacker':
            slack_run()
        elif MACHINE == 'production':
            production_run()
        else:
            normal_run()
    except Exception as e:
        get_logger().info(e)

if __name__ == '__main__':
    main()
